package com.nikitasamantha_10191065.recycleview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import de.hdodenhof.circleimageview.CircleImageView;

public class RvAdapter extends RecyclerView.Adapter <RvAdapter.RvViewHolder> {

    Context context;
    int[] profil;
    String[] nama, pesan, jam;

    public RvAdapter(Context context, int[] profil, String[] nama, String[] pesan, String[] jam) {

        this.context = context;
        this.profil = profil;
        this.nama = nama;
        this.pesan = pesan;
        this.jam = jam;

    }

    @NonNull
    @Override
    public RvViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item, parent, false);
        return new RvViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RvViewHolder holder, int position) {
        holder.profil.setImageResource(profil[position]);
        holder.nama.setText(nama[position]);
        holder.pesan.setText(pesan[position]);
        holder.jam.setText(jam[position]);
    }

    @Override
    public int getItemCount() {
        return nama.length;
    }

    public class RvViewHolder extends RecyclerView.ViewHolder {
        CircleImageView profil;
        TextView nama, pesan, jam;
        public RvViewHolder(@NonNull View itemView) {
            super(itemView);
            profil = itemView.findViewById(R.id.item_profil);
            nama = itemView.findViewById(R.id.item_nama);
            pesan = itemView.findViewById(R.id.item_pesan);
            jam = itemView.findViewById(R.id.item_jam);
        }
    }
}
