package com.nikitasamantha_10191065.recycleview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int[] profil = new int[]{
                R.drawable.arya, R.drawable.eky, R.drawable.cece, R.drawable.mpd, R.drawable.psi, R.drawable.mommy, R.drawable.ndin, R.drawable.feri
        };

        String[] nama = new String[]{
                "Arya Candra", "Sallie Trixie", "Syahriah Syafitri", "MPD A - KEL.3", "PSI - A6", "Mommy", "Andini Dwi", "Feriyanto"
        };

        String[] pesan = new String[]{
                "Oke, otw yah mas",
                "Tengkyuuuuu ekyyyyyy",
                "Sampe situ aja bisa ce",
                "Semangat guysss",
                "Minggu depan aja gimana ?",
                "Sudah mamsss",
                "Iya ndin",
                "Mantap banget per"
        };

        String[] jam = new String[]{
                "16:49", "12:59", "Kemarin", "Kemarin", "Rabu", "Senin", "Sabtu", "Minggu"
        };

        recyclerView = findViewById(R.id.recycleView);
        RvAdapter adapter = new RvAdapter(this,profil, nama, pesan, jam);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}